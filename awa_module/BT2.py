import re,sys,os,glob,subprocess,operator

def buildIndex(arg_index,arg_reference,arg_verbose):
	index_files=glob.glob("{}.*bt2".format(arg_index))
	if(not index_files):
		command="bowtie2-build {} {}".format(arg_reference,arg_index)
		subprocess.Popen(command, shell=True, stderr=subprocess.PIPE).stderr.read()
	else:
		if(arg_verbose):
			sys.stderr.write("! SKIP: it seems that this index has already being build\n")
	return

def alignPairs(arg_index,arg_reads,arg_sam,arg_cpu,arg_verbose):
	sam_file=glob.glob("{}".format(arg_sam))
	if(not sam_file):
		command="bowtie2 -x {} --interleaved {} -S {} -I 0 -X 500 --fr -q --phred33 --no-unal --no-head -D 20 -R 3 -N 0 -L 20 -i S,1,1.15 --rdg 5,3 --rfg 5,3 -p {}".format(arg_index,arg_reads,arg_sam,arg_cpu)
		subprocess.Popen(command, shell=True, stderr=subprocess.PIPE).stderr.read()
	else:
		if(arg_verbose):
			sys.stderr.write("! SKIP: it seems that this alignment has already being done\n")
	return

# def parseRef(arg_reference):
# 	ref=open(arg_reference,"rU")
# 	reference=ref.read()
# 	ref.close()
# 	head,reference=re.compile("(>[^\r\n]+)(.+)",re.MULTILINE|re.DOTALL).findall(reference)[0]
# 	reference=re.sub("\s","",reference)
# 	return reference

def parseSam(reference,arg_sam,arg_verbose):
	length=len(reference)
	cov={} # dictionary for coverage
	con={} # dictionary for connections
	nuc={} # dictionary for nucleotides
	asi={} # stores the alignment score of reads that pass the alignment criteria
	count=0
	with open(arg_sam,"rU") as sam:
		if(arg_verbose):
			sys.stderr.write("//\nREADS\n")
		for line in sam:
			line=line.strip()
			if(line[0]=="@"):
				pass
			elif(line):
##################################################################################
# BOX I:                                                                         #
# End-to-end alignment score example A mismatched base at a high-quality         #
# position in the read receives a penalty of -6 by default. A length-2 read      #
# gap receives a penalty of -11 by default (-5 for the gap open, -3 for the      #
# first extension, -3 for the second extension). Thus, in end-to-end alignment   #
# mode, if the read is 50 bp long and it matches the reference exactly except    #
# for one mismatch at a high-quality position and one length-2 read gap, then    #
# the overall score is -(6 + 11) = -17. The best possible alignment score in     #
# end-to-end mode is 0, which happens when there are no differences between      #
# the read and the reference. See                                                #
# <http://bowtie-bio.sourceforge.net/bowtie2/manual.shtml#scores-higher-more-    #
# similar>.                                                                      #
##################################################################################
##################################################################################
# BOX II:                                                                        #
# AS:i: indicates the alignment score and is present only for aligned reads.     #
# A mismatched base at a high-quality position in the read receives a penalty of #
# -6 by default. A length-2 read gap receives a penalty of -11 by default (-5    #
# for the gap open, -3 for the first extension, -3 for the second extension).    #
# Thus, in end-to-end alignment mode, if the read is 50 bp long and it matches   #
# the reference exactly except for one mismatch at a high-quality position and   #
# one length-2 read gap, then the overall score is -(6 + 11) = -17.              #
# It can be greater than 0 in --local mode (but not in --end-to-end mode). Only  #
# present if SAM record is for an aligned read.                                  #
# When the read is part of a concordantly-aligned pair, this score could be      #
# smaller than the alignment score for the best-scoring alignment found other    #
# than the alignment reported (XS:i).                                            #
##################################################################################
				score=int(re.compile("AS:i:([\+\-\d]+)").findall(line)[0])
				if(score>=-11):
					if(arg_verbose):
						sys.stderr.write("+ Alignment {:,}\n".format(count))
					count+=1
					line=re.compile("([^\s]+)").findall(line)
					start=int(line[3])
					cigar=(line[5])
					seq_crude=line[9]
					qual_crude=line[10]
					seq=""
					qual=""
					for segment in re.compile("(\d+[\w=])").findall(cigar):
						n=int(segment[:-1])
						l=segment[-1]
						if(l=="M" or l=="=" or l=="X"):
							seq+=        seq_crude[:n]
							seq_crude =  seq_crude[n:]
							qual+=       qual_crude[:n]
							qual_crude = qual_crude[n:]
						elif(l=="D" or l=="N"):
							seq+=        "-"
							try:
								qual+=       str(chr(20+33))
							except:
								qual+=       str(unichr(20+33))
						elif(l=="P"):
							seq+=        "*"
							try:
								qual+=       str(chr(20+33))
							except:
								qual+=       str(unichr(20+33))
						elif(l=="I" or l=="S" or l=="H"):
							seq_crude =  seq_crude[n:]
							qual_crude = qual_crude[n:]
						else:
							sys.stderr.write("! ERROR: something bad happened while parsing cigar {} in {}\n".format(cicar,line[0]))
							exit()
# 					if(len(qual_crude)!=0)or(len(seq_crude)!=0):
# 							sys.stderr.write("! ERROR: something bad happened after parsing cigar {} in {}\n".format(cicar,line[0]))
# 							exit()
					length=len(qual)
					for i in range(0,length):
						p=i+start
						if(i==0)or(i==length-1):
							l=0
						else:
							l=1
						if(p in con):
							con[p]+=l
						else:
							con[p]=l
						n=seq[i]
						if(p in nuc):
							nuc[p]+=[n]
						else:
							nuc[p]=[n]
						q=ord(qual[i])-33
						if(p in cov):
							cov[p]+=[q]
						else:
							cov[p]=[q]
						if(p in asi):
							asi[p]+=[score]
						else:
							asi[p]=[score]
				else:
					if(arg_verbose):
						sys.stderr.write("- Alignment {:,}\n".format(count))
					pass
	if(arg_verbose):
		sys.stderr.write("//\n")
	return cov,con,nuc,asi

def report(fragment,cov,con,nuc,asi,arg_verbose):
	if(arg_verbose):
		sys.stdout.write("//\nLOOP\nPosition\tBase in reference\tBase in the majority of the reads\tCoverage\tAvg. quality\tConnection\tAlignment score\n")
	length=len(fragment)
	avg_coverage=[]
	avg_connection=[]
	avg_quality=[]
	avg_score=[]
	match=0
	# Examines 50 bp before and 50 bp after the loop
	min=round(length/2)-50
	max=round(length/2)+50
	for p in range(min,max):
		b=fragment[p-1].upper()
		if(p in nuc):
			n=find_majority(nuc[p]).upper()
			score=float(sum(asi[p]))/float(len(asi[p]))
		else:
			n="?"
			score="?"
		if(b==n):
			match+=1
		else:
			n=n.lower()
		if(p in con):
			l=con[p]
		else:
			l=0
		if(p in cov):
			c=len(cov[p])
			if(c==0):
				q=0
			else:
				q=float(sum(cov[p]))/c
		else:
			c=0
			q=0
		if(l>c):
			sys.stderr.write("! ERROR: something bad hapenned during coverage calculation\n")
			exit()
		else:
			avg_coverage+=[c]
			avg_connection+=[l]
			avg_quality+=[q]
			if(score!="?"):
				avg_score+=[score]
			if(arg_verbose):
				l="{:,}".format(l)
				p="{:,}".format(p)
				c="{:,}".format(c)
				q="{0:.2f}".format(q)
				if(score!="?"):
					score="{0:.2f}".format(score)
				sys.stdout.write("%s\t%s\t%s\t%s\t%s\t%s\t%s\n"%(p,b,n,c,q,l,score))
	if(len(avg_coverage)>=1):
		avg_coverage=float(sum(avg_coverage))/len(avg_coverage)
	else:
		avg_coverage=0.0
	if(len(avg_connection)>=1):
		avg_connection=float(sum(avg_connection))/len(avg_connection)
	else:
		avg_connection=0.0
	if(len(avg_quality)>=1):
		avg_quality=float(sum(avg_quality))/len(avg_quality)
	else:
		avg_quality=0.0
	if(len(avg_score)>=1):
		avg_score=float(sum(avg_score))/len(avg_score)
	else:
		avg_score=0.0
	match=(match/100.0)*100.0
	sys.stdout.write("//\nSTATS\nMatch\t{}%\nAvg. coverage\t{}\nAvg. connection (i.e avg. connectivity)\t{}\nAvg. quality\t{}\nAvg. alignment score\t{}\n//\n".format(match,avg_coverage,avg_connection,avg_quality,avg_score))
	return

def find_majority(k):
	dic={}
	max=0
	for n in k:
		if(n in dic):
			pass
		else:
			count=k.count(n)
			dic[n]=count
			if(count>max):
				max=count
	majority=[key for key in dic if dic[key]==max]
	if(len(majority)==1):
		majority=majority[0]
	elif(set(majority)-set(["A","G","R"])==set([])):
		majority="R"
	elif(set(majority)-set(["C","T","U","Y"])==set([])):
		majority="Y"
	elif(set(majority)-set(["G","C","S"])==set([])):
		majority="S"
	elif(set(majority)-set(["A","T","U","W"])==set([])):
		majority="W"
	elif(set(majority)-set(["G","T","U","K"])==set([])):
		majority="K"
	elif(set(majority)-set(["A","C","M"])==set([])):
		majority="M"
	elif(set(majority)-set(["C","G","T","U","B"])==set([])):
		majority="B"
	elif(set(majority)-set(["A","G","T","U","D"])==set([])):
		majority="D"
	elif(set(majority)-set(["A","C","T","U","H"])==set([])):
		majority="H"
	elif(set(majority)-set(["A","C","G","V"])==set([])):
		majority="V"
	else:
		majority="N"
	return majority
