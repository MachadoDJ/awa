import re,sys

# Calculate coordinates for SelfAlign
def CalcCoords(x,y,n,w,l):
	A = x * (n - w - l)                           # head start
	D = n - w - y                                 # tail start
	E = D + w                                     # tail end
	B,C = sorted( [ (A + w + l) , (E - w - l) ])  # head end and minimun insert size
	return A,B,C,D,E

# Check if alignment meet the stablished criteria
def LoopCriteria(ancestral,k,m):
	# Get first word that meet the criteria
	report=False
	diagonal=""
	for i,j in ancestral:
		if("-" in [i,j]):
			diagonal+="X"
		elif(i==j)and(i in ["A","C","T","G","U"]):
			diagonal+=i
		else:
			diagonal+="N"
	words=[word for word in re.compile("([^X]+)").findall(diagonal) if len(word)>=k]
	for word in words:
		for i in range(0,1+len(word)-k):
			if(word[i:i+k].count("N")<=m):
				report=True
				word=word[i:i+k]
		if(report==True):
			break
	# Get position to clip
	try:
		position2clip=diagonal.index(word)
	except:
		position2clip=0
	return report,position2clip
