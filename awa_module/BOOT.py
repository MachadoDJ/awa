import re,sys,os,glob,subprocess,operator,random



def Fragment(id,seq,p,i):
	bases=["A","C","T","G"]
	filename="{}_{}_{}.bt2.fa".format(re.sub("[^\w\d]+","_",id),p,i)
	if(p==i==0):
		seq=seq
	elif(i<=100):
		mod=""
		x=seq[:50]
		y=seq[len(seq)-50:]
		z=seq[50:len(seq)-50]
		iterate=True
		while iterate==True:
			for char in x:
				if(random.uniform(0.0,1.0)<=p):
					pass
				else:
					mod+=char
			mod+=z
			for char in y:
				if(random.uniform(0.0,1.0)<=p):
					pass
				else:
					mod+=char
			if(mod!=seq):
				iterate=False
				break
			else:
				mod=""
		seq=mod
	elif(i>100):
		mod=""
		x=seq[:50]
		y=seq[len(seq)-50:]
		z=seq[50:len(seq)-50]
		iterate=True
		while iterate==True:
			for char in x:
				coin=random.uniform(0.0,1.0)
				if(coin<=p):
					mod+=char+random.choice(bases)
				else:
					mod+=char
			mod+=z
			for char in y:
				coin=random.uniform(0.0,1.0)
				if(coin<=p):
					mod+=char+random.choice(bases)
				else:
					mod+=char
			if(mod!=seq):
				iterate=False
				break
			else:
				mod=""
		seq=mod
	x=int(len(seq)/2)
	flip=seq[x:]+seq[:x]
	output=open(filename,"w")
	output.write(">{} flipped_fragment {}:{}\n{}\n".format(id,p,i,flip))
	output.close()
	return filename,flip



def buildIndex(arg_index,arg_reference,arg_verbose,p,i):
	command="bowtie2-build {} {}_{}_{} &> /dev/null".format(arg_reference,arg_index,p,i)
	subprocess.Popen(command, shell=True, stderr=subprocess.PIPE).stderr.read()
	return



def alignPairs(arg_index,arg_reads,arg_sam,arg_cpu,arg_verbose,p,i):
	arg_sam=re.sub("\..*","",arg_sam)
	sam_file=glob.glob("{}_{}_{}".format(arg_sam,p,i))
	if(not sam_file):
		command="bowtie2 -x {}_{}_{} --interleaved {} -S {}_{}_{}.sam -I 0 -X 500 --fr -q --phred33 --no-unal --no-head -D 20 -R 3 -N 0 -L 20 -i S,1,1.15 --rdg 5,3 --rfg 5,3 -p {} &> /dev/null".format(arg_index,p,i,arg_reads,arg_sam,p,i,arg_cpu)
		subprocess.Popen(command, shell=True, stderr=subprocess.PIPE).stderr.read()
	else:
		if(arg_verbose):
			sys.stderr.write("! SKIP: it seems that this alignment has already being done\n")
	return



def parseSam(reference,arg_sam,arg_verbose,p,i):
	arg_sam=re.sub("\..*","",arg_sam)
	length=len(reference)
	cov={} # dictionary for coverage
	con={} # dictionary for connections
	nuc={} # dictionary for nucleotides
	asi={} # stores the alignment score of reads that pass the alignment criteria
	count=0
	with open("{}_{}_{}.sam".format(arg_sam,p,i),"rU") as sam:
		if(arg_verbose):
			sys.stderr.write("//\nREADS\n")
		for line in sam:
			line=line.strip()
			if(line[0]=="@"):
				pass
			elif(line):
				score=int(re.compile("AS:i:([\+\-\d]+)").findall(line)[0])
				if(score>=-11):
					if(arg_verbose):
						sys.stderr.write("+ Alignment {:,}\n".format(count))
					count+=1
					line=re.compile("([^\s]+)").findall(line)
					start=int(line[3])
					cigar=(line[5])
					seq_crude=line[9]
					qual_crude=line[10]
					seq=""
					qual=""
					for segment in re.compile("(\d+[\w=])").findall(cigar):
						n=int(segment[:-1])
						l=segment[-1]
						if(l=="M" or l=="=" or l=="X"):
							seq+=        seq_crude[:n]
							seq_crude =  seq_crude[n:]
							qual+=       qual_crude[:n]
							qual_crude = qual_crude[n:]
						elif(l=="D" or l=="N"):
							seq+=        "-"
							try:
								qual+=       str(chr(20+33))
							except:
								qual+=       str(unichr(20+33))
						elif(l=="P"):
							seq+=        "*"
							try:
								qual+=       str(chr(20+33))
							except:
								qual+=       str(unichr(20+33))
						elif(l=="I" or l=="S" or l=="H"):
							seq_crude =  seq_crude[n:]
							qual_crude = qual_crude[n:]
						else:
							sys.stderr.write("! ERROR: something bad happened while parsing cigar {} in {}\n".format(cicar,line[0]))
							exit()
# 					if(len(qual_crude)!=0)or(len(seq_crude)!=0):
# 							sys.stderr.write("! ERROR: something bad happened after parsing cigar {} in {}\n".format(cicar,line[0]))
# 							exit()
					length=len(qual)
					for i in range(0,length):
						p=i+start
						if(i==0)or(i==length-1):
							l=0
						else:
							l=1
						if(p in con):
							con[p]+=l
						else:
							con[p]=l
						n=seq[i]
						if(p in nuc):
							nuc[p]+=[n]
						else:
							nuc[p]=[n]
						q=ord(qual[i])-33
						if(p in cov):
							cov[p]+=[q]
						else:
							cov[p]=[q]
						if(p in asi):
							asi[p]+=[score]
						else:
							asi[p]=[score]
				else:
					if(arg_verbose):
						sys.stderr.write("- Alignment {:,}\n".format(count))
					pass
	if(arg_verbose):
		sys.stderr.write("//\n")
	return cov,con,nuc,asi



def report(fragment,cov,con,nuc,asi,arg_verbose,probability,iteration):
	if(arg_verbose):
		sys.stdout.write("//\n(p={},i={})\nLOOP\nPosition\tBase in reference\tBase in the majority of the reads\tCoverage\tAvg. quality\tConnection\tAlignment score\n".format(probability,iteration))
	length=len(fragment)
	avg_coverage=[]
	avg_connection=[]
	avg_quality=[]
	avg_score=[]
	match=0
	contiguous=True
	# Examines 50 bp before and 50 bp after the loop
	min=round(length/2)-50
	max=round(length/2)+50
	for p in range(min,max):
		b=fragment[p-1].upper()
		if(p in nuc):
			n=find_majority(nuc[p]).upper()
			score=float(sum(asi[p]))/float(len(asi[p]))
		else:
			n="?"
			score="?"
		if(b==n):
			match+=1
		else:
			n=n.lower()
		if(p in con):
			l=con[p]
		else:
			l=0
		if(p in cov):
			c=len(cov[p])
			if(c==0):
				q=0
			else:
				q=float(sum(cov[p]))/c
		else:
			c=0
			q=0
		if(l>c):
			sys.stderr.write("! ERROR: something bad hapenned during coverage calculation\n")
			exit()
		else:
			if(l<=1):
				contiguous=False
			avg_coverage+=[c]
			avg_connection+=[l]
			avg_quality+=[q]
			if(score!="?"):
				avg_score+=[score]
			if(arg_verbose):
				l="{:,}".format(l)
				p="{:,}".format(p)
				c="{:,}".format(c)
				q="{0:.2f}".format(q)
				if(score!="?"):
					score="{0:.2f}".format(score)
				sys.stdout.write("%s\t%s\t%s\t%s\t%s\t%s\t%s\n"%(p,b,n,c,q,l,score))
	if(len(avg_coverage)>=1):
		avg_coverage=float(sum(avg_coverage))/len(avg_coverage)
	else:
		avg_coverage=0.0
	if(len(avg_connection)>=1):
		avg_connection=float(sum(avg_connection))/len(avg_connection)
	else:
		avg_connection=0.0
	if(len(avg_quality)>=1):
		avg_quality=float(sum(avg_quality))/len(avg_quality)
	else:
		avg_quality=0.0
	if(len(avg_score)>=1):
		avg_score=float(sum(avg_score))/len(avg_score)
	else:
		avg_score=0.0
	match=(match/100.0)*100.0
	if(arg_verbose):
		sys.stdout.write("//\nSTATS (p={}, i={})\nMatch\t{}%\nAvg. coverage\t{}\nAvg. connection\t{}\nAvg. quality\t{}\nAvg. alignment score\t{}\n//\n".format(probability,iteration,match,avg_coverage,avg_connection,avg_quality,avg_score))
	return [probability,iteration,contiguous,avg_coverage,avg_connection,avg_quality,avg_score,match]



def find_majority(k):
	dic={}
	max=0
	for n in k:
		if(n in dic):
			pass
		else:
			count=k.count(n)
			dic[n]=count
			if(count>max):
				max=count
	majority=[key for key in dic if dic[key]==max]
	if(len(majority)==1):
		majority=majority[0]
	elif(set(majority)-set(["A","G","R"])==set([])):
		majority="R"
	elif(set(majority)-set(["C","T","U","Y"])==set([])):
		majority="Y"
	elif(set(majority)-set(["G","C","S"])==set([])):
		majority="S"
	elif(set(majority)-set(["A","T","U","W"])==set([])):
		majority="W"
	elif(set(majority)-set(["G","T","U","K"])==set([])):
		majority="K"
	elif(set(majority)-set(["A","C","M"])==set([])):
		majority="M"
	elif(set(majority)-set(["C","G","T","U","B"])==set([])):
		majority="B"
	elif(set(majority)-set(["A","G","T","U","D"])==set([])):
		majority="D"
	elif(set(majority)-set(["A","C","T","U","H"])==set([])):
		majority="H"
	elif(set(majority)-set(["A","C","G","V"])==set([])):
		majority="V"
	else:
		majority="N"
	return majority
