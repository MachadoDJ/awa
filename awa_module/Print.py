import re,sys,os

# Print alignment matrix in human readable format
def Matrix(matrix,X,Y):
	x=[]
	y=[]
	for coord in matrix:
		col,line=coord.split(":") # Matrix coordinates are organized as column:line (or x-axis:y-axis)
		x+=[int(col)]
		y+=[int(line)]
	x=sorted(list(set(x)))
	y=sorted(list(set(y)))
	text=""
	for col in x:
		text+=" {}".format(X[col])
	text+="\n"
	for line in y:
		values=[]
		for col in x:
			values+=[matrix["{}:{}".format(col,line)]]
		values=[str(value) for value in values]
		text+="{} {}\n".format(Y[line]," ".join(values))
	sys.stderr.write(text)
	return

# Prints the alignment in a human-readable format
def Alignment(ancestral):
	chunck=0
	increments=60
	matchs=0
	sys.stdout.write("// Begin alignment //\n")
	while chunck<=len(ancestral)-1:
		segment=ancestral[chunck:chunck+increments]
		for i in segment:
			if(i[0]==i[1]):
				sys.stdout.write("{}".format(i[0].upper()))
				matchs+=1
			else:
				sys.stdout.write("{}".format(i[0].lower()))
		sys.stdout.write("\n")
		for i in segment:
			if(i[0]==i[1]):
				sys.stdout.write("{}".format(i[1].upper()))
			else:
				sys.stdout.write("{}".format(i[1].lower()))
		sys.stdout.write("\n")
		chunck+=increments
	sys.stdout.write("// End alignment //\n")
	return

# Trim the input sequence and return the potential circular sequence within it
def Clip(ancestral,body,position2clip): # Extract the region inside the loop
	clip=""
	for i in range(0,len(ancestral)):
		if(i>=position2clip):
			nuc=ancestral[i][0]
			if(nuc!="-"):
				clip+=nuc
	clip+=body
	for i in range(0,len(ancestral)):
		if(i<position2clip):
			nuc=ancestral[i][1]
			if(nuc!="-"):
				clip+=nuc
	return clip

# Save flipped fragment
def Fragment(id,seq):
	filename="{}.bt2.fa".format(re.sub("[^\w\d]+","_",id))
	if os.path.isfile(filename):
		sys.stderr.write("! ERROR: file {} already exists.\n".format(filename))
		exit()
	else:
		x=int(len(seq)/2)
		flip=seq[x:]+seq[:x]
		output=open(filename,"w")
		output.write(">{} flipped_fragment\n{}\n".format(id,flip))
		output.close()
	return filename,flip
