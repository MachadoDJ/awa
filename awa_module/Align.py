from awa_module import Special

# Find matching word from sequence tail on sequence head
def MatchString(seq,head,tail,A,B,C,D,E,x,y,n,w,l,p):
	while D>C:           # loop for head
		while D>C:       # loop for tail
			if C>=D:
				break
			elif tail in head:
				p=head.index(tail)
				break
			else:
				y+=1
				A,B,C,D,E=Special.CalcCoords(x,y,n,w,l)
				tail=seq[D:E]
		if(p>=0):
			break
		else:
			x+=1
			y=0
			A,B,C,D,E=Special.CalcCoords(x,y,n,w,l)
			head=seq[A:B]
			tail=seq[D:E]
			if(C>=D):
				break
	return x,y,n,w,l,p


# Calculate alignment matrix
# def Downpass(X,Y):
# 	matrix={}      # Stores alignment costs
# 	M={}
# 	D={}
# 	# Down-pass
# 	for col in range(0,len(X)):
# 		for line in range(0,len(Y)):
# 			# Get nucleotides to compare
# 			x=X[col]
# 			y=Y[line]
# 			# Costs
# 			c1=3 # match
# 			c2=2 # indel
# 			c3=3 # match open
# 			c4=2 # indel open
# 			# Get coordinates
# 			coord ="{}:{}".format(col,   line  ) # current coordinate
# 			tcoord="{}:{}".format(col,   line-1) # top coordinate
# 			dcoord="{}:{}".format(col-1, line-1) # diagonal coordinate
# 			lcoord="{}:{}".format(col-1, line  ) # left coordinate
# 			# Calculates distance
# 			dist=0 # Match = +3, ambiguous = 0 else = -3
# 			if (x==y):
# 				dist+=c1
# # 				elif ((x=="R") and (y=="A" or y=="G")) or ((x=="Y") and (y=="C" or y=="T" or y=="U")) or ((x=="S") and (y=="G" or y=="C")) or ((x=="W") and (y=="A" or y=="T" or y=="U")) or ((x=="K") and (y=="G" or y=="T" or y=="U")) or ((x=="M") and (y=="A" or y=="C")) or ((x=="B") and (y=="C" or y=="G" or y=="T" or y=="U")) or ((x=="D") and (y=="A" or y=="G" or y=="T" or y=="U")) or ((x=="H") and (y=="A" or y=="C" or y=="T" or y=="U")) or ((x=="V") and (y=="A" or y=="C" or y=="G")) or ((x=="N") and (y=="A" or y=="C" or y=="G" or y=="T" or y=="U")) or ((y=="R") and (x=="A" or x=="G")) or ((y=="Y") and (x=="C" or x=="T" or x=="U")) or ((y=="S") and (x=="G" or x=="C")) or ((y=="W") and (x=="A" or x=="T" or x=="U")) or ((y=="K") and (x=="G" or x=="T" or x=="U")) or ((y=="M") and (x=="A" or x=="C")) or ((y=="B") and (x=="C" or x=="G" or x=="T" or x=="U")) or ((y=="D") and (x=="A" or x=="G" or x=="T" or x=="U")) or ((y=="H") and (x=="A" or x=="C" or x=="T" or x=="U")) or ((y=="V") and (x=="A" or x=="C" or x=="G")) or ((y=="N") and (x=="A" or x=="C" or x=="G" or x=="T" or x=="U")):
# # 					dist+=0
# 			else:
# 				dist-=c1
# 			# Fill the first column and first line with zeros
# 			if(col==0)or(line==0):
# 				matrix[coord]  = 0                         # Start with [column 0; line 0] =  0
# 				M[coord]       = 0
# 				D[coord]       = 0
# 			# Fill the rest of the alignment matrix (gap cost = -2)
# 			else:
# 				top      = matrix[tcoord] - c2    #+ D[tcoord]
# 				diagonal = matrix[dcoord] + dist  #+ M[dcoord]
# 				left     = matrix[lcoord] - c2    #+ D[lcoord]
# 				# In case the distance is negative, replace by zero
# # 					if(max(top,diagonal,left)<0):
# # 						matrix[coord]=0
# # 					else:
# # 						matrix[coord]=max(top,diagonal,left)
# 				matrix[coord]=max(top,diagonal,left)
# 				if(max(top,diagonal,left)==diagonal):
# 					M[coord]=  c3
# 					D[coord]= -c4
# 				else:
# 					M[coord]= -c3
# 					D[coord]=  c4
# 	return matrix,col,line

# Align the two descendant sequences based on the matrix from Downpass
# def Uppass(X,Y,matrix,col,line):
# 	ancestral=[]
# 	while not col==line==0:
# 		x=X[col]
# 		y=Y[line]
# 		coord="{}:{}".format(col,line)
# 		if  (line==0):  # Can only go left
# 			ancestral+=[ [x, "-"] ]
# 			col-=  1
# 		elif(col ==0):  # Can only go up
# 			ancestral+=[ ["-", y] ]
# 			line-= 1
# 		else:           # Decide were to go
# 			up       = matrix["{}:{}".format(col,   line-1)]
# 			diagonal = matrix["{}:{}".format(col-1, line-1)]
# 			left     = matrix["{}:{}".format(col-1, line)]
# 			# Order of preference = diagonal > left > up
# 			if   (max(up,diagonal,left)==diagonal):
# 				ancestral+=[ [x, y] ]
# 				col-=  1
# 				line-= 1
# 			elif (max(up,diagonal,left)==up):
# 				ancestral+=[ ["-", y] ]
# 				line-= 1
# 			elif (max(up,diagonal,left)==left):
# 				ancestral+=[ [x, "-"] ]
# 				col-=  1
# 	return ancestral
