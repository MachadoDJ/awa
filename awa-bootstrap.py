#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# awa-bootstrap.py

# Given a reference sequence in FASTA format and short sequence reads in FASTA
# format, check if there is evidence for circularity.

# Phred score: # Sanger format can encode a Phred quality score from 0 to 93 using ASCII
# 33 to 126. Illumina's newest version (1.8) of their pipeline CASAVA will directly
# produce fastq in Sanger format.

# Columns in SAM file from Bowtie2 v2.3.1: the third column is the start position and the
# tenth column is sequence quality. We only need these two columns here.

# Import modules and libraries
import argparse,glob,operator,os,re,subprocess,sys
try:
	from awa_module import Read,Print,Align,Special,BOOT
except ImportError:
	sys.stderr.write("! ERROR: failed to import awa_module\n")
	exit()

# Set arguments
parser=argparse.ArgumentParser()

# Required
parser.add_argument("-i","--index",help="Index name (Bowtie2 >= v.2.3.1)",type=str,required=True)
parser.add_argument("-f","--fragment",help="Consensus sequence in FASTA format",type=str,required=True)
parser.add_argument("-r","--reads",help="Interleaved paired-end reads in FASTQ format (CASAVA +1.8)",type=str,required=True)
parser.add_argument("-s","--sam",help="Alignment in SAM format (Bowtie2 >= v.2.3.1)",type=str,required=True)

# Not required (have default values)
parser.add_argument("-p","--processors",help="Maximum number of CPUs to use",type=int,default=4,required=False)
parser.add_argument("-v","--verbose", help="Increase output verbosity",action="store_true",required=False)
args=parser.parse_args()

# Check arguments
description,fragment=Read.Reference(args.fragment)

# Define functions
def selfAlign(sequence,w,l,n): # Aligns the sequence to itself, to see if it forms a circle
	x=0                   # iterations over head
	y=0                   # iterations over tail
	p=-1                  # start position on head
	A,B,C,D,E=Special.CalcCoords(x,y,n,w,l)
	head=sequence[A:B]
	tail=sequence[D:E]
	x,y,n,w,l,p=Align.MatchString(sequence,head,tail,A,B,C,D,E,x,y,n,w,l,p)
	if(p>0):
		frag=sequence[x*(n-w-l)+p:n-w-y]
		word=sequence[n-w-y:n-y]
		sys.stdout.write(">word {}:{}\n{}\n".format(n-w-y,n-y,word))
		sys.stdout.write(">fragment {}:{}\n{}\n".format(x*(n-w-l)+p,n-w-y,frag))
	else:
		frag=""
		sys.stderr.write("- No match :-(\n")
	return frag

# Default
sys.stdout.write("Indel Prob.\tIteration\tContiguity\tCoverage\tConnectivity\tQuality\tScore\tSimilarity\n")
qual={}
p=0
i=0
ref_filename,fragment=BOOT.Fragment(description,fragment,p,i)
BOOT.buildIndex(args.index,ref_filename,args.verbose,p,i)
BOOT.alignPairs(args.index,args.reads,args.sam,args.processors,args.verbose,p,i)
cov,con,nuc,asi=BOOT.parseSam(fragment,args.sam,args.verbose,p,i)
qual=BOOT.report(fragment,cov,con,nuc,asi,args.verbose,p,i)
command="rm {} {}_{}_{}*".format(ref_filename,args.index,p,i)
subprocess.Popen(command, shell=True, stderr=subprocess.PIPE).stderr.read()
sys.stdout.write("{}\n".format("\t".join([str(i) for i in qual])))
sys.stdout.flush()
# Iterations
for p in [0.01,0.05]:
	for i in range(1,201):
		ref_filename,fragment=BOOT.Fragment(description,fragment,p,i)
		BOOT.buildIndex(args.index,ref_filename,args.verbose,p,i)
		BOOT.alignPairs(args.index,args.reads,args.sam,args.processors,args.verbose,p,i)
		cov,con,nuc,asi=BOOT.parseSam(fragment,args.sam,args.verbose,p,i)
		qual=BOOT.report(fragment,cov,con,nuc,asi,args.verbose,p,i)
		command="rm {} {}_{}_{}* {}*".format(ref_filename,args.index,p,i,re.sub("\..*","",args.sam))
		subprocess.Popen(command, shell=True, stderr=subprocess.PIPE).stderr.read()
		sys.stdout.write("{}\n".format("\t".join([str(i) for i in qual])))
		sys.stdout.flush()

# Quit
exit()
