AWA - Copyright (C) 2017 - Denis Jacob Machado

version 20170413

# i. AVAILABILITY

AWA comes with ABSOLUTELY NO WARRANTY!
AWA is available at and https://gitlab.com/MachadoDJ/awa.

The programs may be freely used, modified, and shared under the GNU General Public License version 3.0 (GPL-3.0, http://opensource.org/licenses/GPL-3.0).

See LICENSE.txt for complete information about terms and conditions.

Also, check the wiki page that are available at https://gitlab.com/MachadoDJ/awa/wikis/home/.

# ii. CONTACT

* Author: Denis Jacob Machado
* Homepage: http://about.me/machadodj
* Email: dmachado [at] uncc [dot] [edu]
* Laboratório de Anfíbios (USP, Brazil): http://www.ib.usp.br/grant/anfibios
* Janies'Lab (UNC Charlotte, USA): https://janieslab.github.io/

# iii. SUMMARY

The “AWA” project integrates software solutions for testing the completeness of assemblies of circular DNA based on short sequence reads. A new version of AWA is currently being prepared and will accompany a discussion of its applications for all types of circular DNA. The current version should be considered a draft and has only been tested for mitogenome assemblies of frog DNA.

# 1. DEPENDENCIES

AWA was written in Python v3.5.1 on a macOS v10.12.4 and hasnot being tested in other platforms. The program should run on any UNIX based system, but contact the author if you have problems.

The program `awa-trim` can be used as a stand-alone application.

The programs `awa-map` and awa-bootstrap require a working installation of Bowtie2 (see http://bowtie-bio.sourceforge.net/bowtie2/index.shtml).

# 2. BEFORE START USING AWA

Keep in mind this is beta software that was designed veryspecifically to test the circularity of mitogenomes assemblies performed with MITObim (https://github.com/chrishah/MITObim).

Reads must be provided as `.fastq` file. **Entries must be interleave**.

Reference sequence must be provided as a single `.fasta`
file.

# 3. WHAT IS INSIDE

The program `awa-trim` searches for the largest possible circular DNA fragment inside the presented scaffold by means of searching from matching words with a minimum length that are at a minimum distance from each other.

The program `awa-map` leverages on Bowtie2. It maps the reads back to the potential circular DNA fragment, flipping the fragment so its original end will be together at the middle of the fragment. The program reports many statistics focused on the 100 pb in the middle of the flipped sequence fragment. All this stats are know, except for the contiguity index, which measures the amount of reads that supports the position of a nucleotides in relation to its two adjacent neighbors. The program will not say if the DNA is circular, it will only provide information necessary for the user to make that decision based on his own criteria.

The program `awa-bootstrap` is used by developers to see how sensitive the program is to random deletion or addition of bases on the 100 pb in the middle of the flipped sequence.

# 4. INSTALL

As it is, download AWA scripts and the `awa_module` folder to your computer, make the scripts executable, and add them to your `PATH`. Make sure the `awa_module` folder is in the same directory as the three AWA scripts.

# 5. HOW TO USE IT

See simple tutorial available at AWA's Wiki: https://gitlab.com/MachadoDJ/awa/wikis/Tutorial.

# CITE

Jacob Machado, D., Janies, D., Brouwer, C., & Grant, T. (2018). A new strategy to infer circularity applied to four new complete frog mitogenomes. Ecology and Evolution, 8 (8), 4011-4018. DOI: https://doi.org/10.1002/ece3.3918
