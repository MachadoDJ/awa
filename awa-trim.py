#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# awa-trim.py

# Given a reference sequence in FASTA format and short sequence reads in FASTA
# format, check if there is evidence for circularity.

# The first program of two, awa-trim.py, finds identical words (k-mers) at a
# minimum distance from each other (i.e., minimum insert size or sequence
# length).

# Import modules and libraries
import argparse,sys
try:
	from awa_module import Read
except ImportError:
	sys.stderr.write("! ERROR: failed to import awa_module\n")
	exit()

# Set arguments
parser=argparse.ArgumentParser()
parser.add_argument("-s","--sequence",help="Consensus sequence in FASTA format",type=str,required=True)
parser.add_argument("-w","--word",help="Word size (default = 10)",type=int,default=10,required=False)
parser.add_argument("-l","--length",help="Minimum insert size (default = 1000)",type=int,default=1000,required=False)
parser.add_argument("-o","--output",help="Output file (default = output.fasta)",type=str,default="output.fasta",required=False)
args=parser.parse_args()

# Define functions
def findMatch(sequence,length):
	window=10
	longest_match=0
	while (length-window)>=args.length:
		x=sequence[length-window:]
		y=sequence[:window]
		match_string="".join([x[n] if x[n]==y[n] else " " for n in range(0,window)]).split()
		if(match_string):
			longest_match=sorted([len(x) for x in match_string])[-1]
			if(longest_match>=args.word):
				stop=printFragment(sequence,length,window,match_string,longest_match)
				if(stop):
					break
				else:
					pass
			else:
				pass
		window+=1
	return

def printFragment(sequence,length,window,match_string,longest_match):
	for x in match_string:
		if(len(x)==longest_match):
			break
	i=length-window+sequence[length-window:].index(x)
	j=sequence[:window].index(x)
	if((i-j)>=args.length):
		sys.stdout.write("Found a match from {}:{} (length {})\n".format(j,i,i-j))
		output=open(args.output,"w")
		output.write(">fragment {}:{}\n{}\n".format(j,i,sequence[j:i]))
		output.close()
		stop=True
	else:
		stop=False
	return stop

# Execute functions : find possible circular fragment
sys.stderr.write("- Parse consensus sequence\n")
description,sequence=Read.Reference(args.sequence) # Returns sequence description and uppercase sequence
length=len(sequence)
findMatch(sequence,length)
sys.stderr.write("- Finish self-aligning\n")

# Quit
exit()
