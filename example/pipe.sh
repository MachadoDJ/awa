#!/bin/bash
set -o errexit

function part1 {
	for k in 13 15 17 19 21 23 25 27 29 31 ; do
		python3 awa-trim.py -s consensus.fa -w ${k} -l 15000 -o example_${k}mer.fa
	done
}

function part2 {
	for f in example_*mer.fa ; do
		k=$(echo ${f} | grep -o -E "[0-9]+")
		python3 awa-map.py -i index_${k}mer -f example_${k}mer.fa -r reads.fq -s alignment_${k}mer.sam -p 4 -v &> log_${k}mer.log
	done
}

part1 # Clip putative circular sequences from within the given scaffold
part2 # Get stats for each putative fragment

exit
